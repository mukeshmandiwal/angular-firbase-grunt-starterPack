# angular firebase grunt staterPack
***

This project is generated with [yo angular generator](https://github.com/yeoman/generator-angular)
version 0.11.1.

## Install Node.js and Npm.

I hope you have node.js installed on your system.

for npm install `npm install` on your directory.

## For Grunt install.

for grunt globel install `npm install -g grunt-cli`

for grunt local install `npm install grunt --save-dev` .

for the bower install `install bower`.

and also check other dependency injection required `npm install <required module> --save-dev`.

## Build & development

Run `grunt` for building and `grunt serve` for preview.

## Testing

Running `grunt test` will run the unit tests with karma.
